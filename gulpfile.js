// Load plugins
const gulp = require("gulp");
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const uglifycss = require('gulp-uglifycss');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const merge = require('gulp-merge');
const autoprefixer = require("gulp-autoprefixer");
const less = require('gulp-less');

function css() {
	return gulp
		.src('./css/less/main.less')
		.pipe(sourcemaps.init())
		.pipe(less())
		.pipe(autoprefixer())
		.pipe(uglifycss({
			"max-line-len": 80
		}))
		.pipe(sourcemaps.write('maps'))
		.pipe(gulp.dest('./css/'));
}

function js() {
	return gulp.src([
		'libs/bootstrap.bundle.js',
		'libs/main.js'
		],
		{base: 'libs/'})
	.pipe(sourcemaps.init())
	.pipe(concat('site.js'))
	.pipe(uglify())
	.pipe(sourcemaps.write('maps'))
	.pipe(gulp.dest('./js/'));
}

function watch() {
	gulp.watch('./css/less/**/*.less', css);
	gulp.watch('./libs/*.js', js);
}

// define complex tasks
const build = gulp.parallel(css, js);

// export tasks
exports.css = css;
exports.js = js;
exports.watch = watch;
exports.build = build;
exports.default = build;
