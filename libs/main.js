/**
 * As part of the Drupal 8 Javascript Coding Standards, 
 * all of the javascript code must be declared inside a closure wrapping the whole file. 
 * This closure must be in strict mode.
 * 
 * ESLint is used to ensure our JavaScript code is consistent 
 * and free from syntax error and leaking variables and 
 * that it can be properly minified.
 * @see https://www.drupal.org/node/1955232
 * @see http://eslint.org/docs/user-guide/command-line-interface
 */
(function ($) {
	'use strict';
	
	console.log('Playground master theme initialized!');
	
})(jQuery);