This is a custom theme to understand D8 theming.
You can use this as a bootstrap theme. It's setup to use the Twitter Bootstrap framework and 
integrates Gulp/less for CSS development.

-- Jose D'Silva (@Bombayworks Software Solutions)
