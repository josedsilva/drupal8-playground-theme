import gulp from "gulp";
const {task, src, dest, watch, series, parallel} = gulp;

import sourcemaps from 'gulp-sourcemaps';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import uglifycss from 'gulp-uglifycss';
import less from 'gulp-less';
import autoprefixer from "gulp-autoprefixer";

task('css', () => {
  return src('./src/less/main.less')
  .pipe(sourcemaps.init())
  .pipe(less())
  .pipe(autoprefixer())
  .pipe(uglifycss({"max-line-len": 80}))
  .pipe(concat('style.css'))
  .pipe(sourcemaps.write('maps'))
  .pipe(dest('./dist/css/'));
});

task('js', () => {
  return src('./src/scripts/*.js')
  .pipe(sourcemaps.init())
  .pipe(uglify())
  .pipe(sourcemaps.write('maps'))
  .pipe(dest('./dist/js/'));
});

task('watch', () => {
  watch('./src/less/**/*.less', series('css'));
  return watch('./src/scripts/*.js', series('js'));
});

task('default', parallel('css', 'js'));
